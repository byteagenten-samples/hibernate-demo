package com.example.hibernatedemo.controller;

import com.example.hibernatedemo.domain.DemoUser;
import com.example.hibernatedemo.domain.UserRole;
import com.example.hibernatedemo.domain.dto.DDemoUser;
import com.example.hibernatedemo.domain.dto.DUserRole;
import com.example.hibernatedemo.domain.dto.SimpleDemoUser;
import com.example.hibernatedemo.service.DemoUserService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class DemoUserController {

	private final ModelMapper mapper;

	private final DemoUserService demoUserService;

	public DemoUserController(ModelMapper mapper, DemoUserService demoUserService) {
		this.mapper = mapper;
		this.demoUserService = demoUserService;
	}

	@GetMapping("")
	public List<DDemoUser> getUsers() {
		var listType = new TypeToken<List<DDemoUser>>() {}.getType();

		return mapper.map(demoUserService.getAllUsers(), listType);
	}

	@GetMapping("/filter")
	public List<DDemoUser> getUsersBy(@RequestParam String firstName) {
		var listType = new TypeToken<List<DDemoUser>>() {}.getType();

		return mapper.map(demoUserService.findByFirstName(firstName), listType);
	}

	@GetMapping("/simple")
	public List<SimpleDemoUser> getSimpleUsers() {
		return demoUserService.findAllAsSimpleDemoUsers();
	}

	@PostMapping("/add")
	public DDemoUser addDemoUser(@RequestBody DDemoUser demoUser) {
		var inputUser = mapper.map(demoUser, DemoUser.class);

		return mapper.map(demoUserService.addUser(inputUser), DDemoUser.class);
	}

	@PostMapping("/role/add")
	public DUserRole addDemoUser(@RequestBody DUserRole userRole) {
		var inputRole = mapper.map(userRole, UserRole.class);

		return mapper.map(demoUserService.addUserRole(inputRole), DUserRole.class);
	}
}
