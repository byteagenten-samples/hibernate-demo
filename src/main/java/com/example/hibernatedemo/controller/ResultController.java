package com.example.hibernatedemo.controller;

import com.example.hibernatedemo.domain.dto.DResult;
import com.example.hibernatedemo.service.ResultService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/result")
public class ResultController {

	private final ModelMapper mapper;

	private final ResultService resultService;

	public ResultController(ModelMapper mapper, ResultService resultService) {
		this.mapper = mapper;
		this.resultService = resultService;
	}

	@GetMapping("")
	public List<DResult> getResults() {
		var listType = new TypeToken<List<DResult>>() {}.getType();

		return mapper.map(resultService.getAllResults(), listType);
	}

	@GetMapping("/random/{countResultInfos}")
	public String insertAndGetRandomResults(@PathVariable int countResultInfos) {
		return resultService.insertRandomResults(countResultInfos);
	}

	@GetMapping("/load")
	public List<DResult> loadResults(@RequestParam boolean optimized) {
		var listType = new TypeToken<List<DResult>>() {}.getType();

		if (optimized) {
			return mapper.map(resultService.getAllResultsOptimized(), listType);
		}
		return mapper.map(resultService.getAllResults(), listType);
	}
}
