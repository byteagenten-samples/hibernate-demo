package com.example.hibernatedemo.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Result {

	@Id
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "pooled-lo"
	)
	@GenericGenerator(
			name = "pooled-lo",
			strategy = "sequence",
			parameters = {
					@Parameter(name = "initial_value", value = "1"),
					@Parameter(name = "increment_size", value = "100"),
					@Parameter(name = "optimizer", value = "pooled-lo"),
					@Parameter(name = "prefer_sequence_per_entity", value = "true"),
			}
	)
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private Date calculationTime;
	private float value;

	@OneToMany(mappedBy = "result", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<ResultInfo> resultInfos = new HashSet<>();
}
