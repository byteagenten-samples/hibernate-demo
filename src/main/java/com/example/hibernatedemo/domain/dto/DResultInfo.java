package com.example.hibernatedemo.domain.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DResultInfo {
	private long id;

	private String description;
}
