package com.example.hibernatedemo.domain.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class DDemoUser {
	private long id;

	private String firstName;
	private String lastName;
	private String email;
	private boolean active;

	private List<DUserRole> userRoles;
}
