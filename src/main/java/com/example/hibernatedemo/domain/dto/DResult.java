package com.example.hibernatedemo.domain.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class DResult {
	private long id;

	private Date calculationTime;
	private float value;

	private Set<DResultInfo> resultInfos = new HashSet<>();
}
