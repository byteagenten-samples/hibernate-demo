package com.example.hibernatedemo.domain.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DUserRole {
	private long id;

	private String name;
}
