package com.example.hibernatedemo.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class ResultInfo {

	@Id
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "pooled-lo"
	)
	@GenericGenerator(
			name = "pooled-lo",
			strategy = "sequence",
			parameters = {
					@Parameter(name = "initial_value", value = "1"),
					@Parameter(name = "increment_size", value = "100"),
					@Parameter(name = "optimizer", value = "pooled-lo"),
					@Parameter(name = "prefer_sequence_per_entity", value = "true"),
			}
	)
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne
	private Result result;

	private String description;
}
