package com.example.hibernatedemo.service;

import com.example.hibernatedemo.domain.Result;
import com.example.hibernatedemo.domain.ResultInfo;
import com.example.hibernatedemo.repository.ResultRepository;
import com.example.hibernatedemo.util.PerformanceLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

@Service
public class ResultService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ResultService.class);

	private final ResultRepository resultRepository;

	private final Random random = new Random();

	private ResultInfo generateRandomResultInfo(Result result) {
		var resultInfo = new ResultInfo();
		resultInfo.setDescription("test");
		resultInfo.setResult(result);
		return resultInfo;
	}

	private Result generateRandomResult(int countResultInfos) {
		var result = new Result();

		result.setCalculationTime(new Date());
		result.setValue(random.nextFloat());

		var resultInfoSet = new HashSet<ResultInfo>();

		for(var i= 0; i<countResultInfos; i++) {
			resultInfoSet.add(generateRandomResultInfo(result));
		}

		result.setResultInfos(resultInfoSet);

		return result;
	}

	public ResultService(ResultRepository resultRepository) {
		this.resultRepository = resultRepository;
	}

	public List<Result> getAllResults() {
		return resultRepository.findAll();
	}

	public List<Result> getAllResultsOptimized() {
		return resultRepository.findAllWithResultInfos();
	}

	@Transactional
	public String insertRandomResults(int countResultInfos) {
		List<Result> resultList = new ArrayList<>();

		for(var i=0; i<50; i++) {
			resultList.add(generateRandomResult(countResultInfos));
		}

		var start = Instant.now();

		resultRepository.saveAllAndFlush(resultList);

		var duration = PerformanceLog.generateDurationString(start);
		LOGGER.info("estimated time {}", duration);

		return duration;
	}
}
