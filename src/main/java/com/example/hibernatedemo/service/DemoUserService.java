package com.example.hibernatedemo.service;

import com.example.hibernatedemo.domain.DemoUser;
import com.example.hibernatedemo.domain.UserRole;
import com.example.hibernatedemo.domain.dto.SimpleDemoUser;
import com.example.hibernatedemo.repository.DemoUserRepository;
import com.example.hibernatedemo.repository.UserRoleRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DemoUserService {

	private final DemoUserRepository demoUserRepository;
	private final UserRoleRepository userRoleRepository;

	public DemoUserService(DemoUserRepository demoUserRepository, UserRoleRepository userRoleRepository) {
		this.demoUserRepository = demoUserRepository;
		this.userRoleRepository = userRoleRepository;
	}

	public List<DemoUser> getAllUsers() {
		return demoUserRepository.findAll();
	}

	public DemoUser addUser(DemoUser demoUser) {
		return demoUserRepository.save(demoUser);
	}

	public UserRole addUserRole(UserRole userRole) {
		return userRoleRepository.save(userRole);
	}

	public List<DemoUser> findByFirstName(String firstName) {
		return demoUserRepository.findDemoUserByFirstName(firstName);
	}

	public List<SimpleDemoUser> findAllAsSimpleDemoUsers() {
		return demoUserRepository.findAllAndMapToSimpleUser();
	}
}
