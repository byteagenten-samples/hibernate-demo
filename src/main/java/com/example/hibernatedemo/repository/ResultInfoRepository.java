package com.example.hibernatedemo.repository;

import com.example.hibernatedemo.domain.ResultInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResultInfoRepository extends JpaRepository<ResultInfo, Long> {
}
