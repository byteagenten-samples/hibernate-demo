package com.example.hibernatedemo.repository;

import com.example.hibernatedemo.domain.DemoUser;
import com.example.hibernatedemo.domain.dto.SimpleDemoUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DemoUserRepository extends JpaRepository<DemoUser, Long> {

	List<DemoUser> findDemoUserByFirstNameIsLike(String firstName);

	@Query("SELECT DISTINCT d from DemoUser d LEFT JOIN FETCH d.userRoles u WHERE d.firstName like :firstName")
	List<DemoUser> findDemoUserByFirstName(@Param("firstName") String firstName);

	@Query("SELECT new com.example.hibernatedemo.domain.dto.SimpleDemoUser(d.firstName, d.lastName) FROM DemoUser d")
	List<SimpleDemoUser> findAllAndMapToSimpleUser();
}
