package com.example.hibernatedemo.repository;

import com.example.hibernatedemo.domain.Result;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultRepository extends JpaRepository<Result, Long> {

	@Query("SELECT DISTINCT r from Result r LEFT JOIN FETCH r.resultInfos ri")
	List<Result> findAllWithResultInfos();
}
