package com.example.hibernatedemo.util;

import java.time.Duration;
import java.time.Instant;

public class PerformanceLog {

	public static String generateDurationString(Instant start) {
		var duration = Duration.between(start, Instant.now());
		return String.format("%02d:%02d.%03d mm:ss.SSS", duration.toMinutes(), duration.toSecondsPart(), duration.toMillisPart());
	}
}
